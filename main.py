from __future__ import print_function
import os
from dataclasses import dataclass, MISSING

import hydra
import torch
import torch.nn.functional as F
from hydra.utils import instantiate
from torchvision import datasets, transforms
from torch.optim.lr_scheduler import StepLR

from mnist_bins.trainer import train, test

torch.optim.Adadelta

ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__)))

@dataclass
class Config:
    batch_size: int = MISSING
    test_batch_size: int = MISSING
    epochs: int = MISSING
    lr: float = MISSING
    gamma: float = MISSING
    no_cuda: bool = MISSING
    dry_run: bool = MISSING
    seed: int = MISSING
    log_interval: int = MISSING
    save_model: bool = MISSING

def run_(cfg, seed)->float:
    torch.manual_seed(seed)
    use_cuda = not cfg.no_cuda and torch.cuda.is_available()

    device = torch.device("cuda" if use_cuda else "cpu")

    train_kwargs = {'batch_size': cfg.batch_size}
    test_kwargs = {'batch_size': cfg.test_batch_size}
    if use_cuda:
        cuda_kwargs = {'num_workers': 1,
                       'pin_memory': True,
                       'shuffle': True}
        train_kwargs.update(cuda_kwargs)
        test_kwargs.update(cuda_kwargs)

    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.1307,), (0.3081,))
    ])
    dataset1 = datasets.MNIST('../data', train=True, download=True,
                              transform=transform)
    dataset2 = datasets.MNIST('../data', train=False,
                              transform=transform)
    train_loader = torch.utils.data.DataLoader(dataset1, **train_kwargs)
    test_loader = torch.utils.data.DataLoader(dataset2, **test_kwargs)

    model = instantiate(cfg.model).to(device)
    optimizer = instantiate(cfg.optim.optimizer, params=model.parameters())

    scheduler = StepLR(optimizer, step_size=1, gamma=cfg.gamma)
    callbacks = []  # [lambda _: test(model, device, test_loader)]

    for epoch in range(1, cfg.epochs + 1):
        train(cfg, model, device, train_loader, optimizer, epoch, with_closure=cfg.optim.with_closure,
              callbacks=callbacks)
        test(model, device, test_loader)
        scheduler.step()

    if cfg.save_model:
        torch.save(model.state_dict(), "mnist_cnn.pt")

    return test(model, device, test_loader)

@hydra.main(config_path=f'{ROOT_DIR}/config', config_name='config')
def main(cfg: Config) -> float:
    # Training settings
    seeds = list(range(cfg.seeds))
    seed_results = []
    for seed in seeds:
        result = run_(cfg, seed)
        seed_results.append(result)

    print(seed_results)
    return seed_results


if __name__ == '__main__':
    main()