import math

import torch

import torch.nn as nn
import torch.nn.functional as F
from torch import Tensor
from torch.nn import Parameter, init


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 32, 3, 1)
        self.conv2 = nn.Conv2d(32, 64, 3, 1)
        self.dropout1 = nn.Dropout(0.25)
        self.dropout2 = nn.Dropout(0.5)
        self.fc1 = nn.Linear(9216, 128)
        self.fc2 = nn.Linear(128, 10)

    def forward(self, x):
        x = self.conv1(x)
        x = F.relu(x)
        x = self.conv2(x)
        x = F.relu(x)
        x = F.max_pool2d(x, 2)
        x = self.dropout1(x)
        x = torch.flatten(x, 1)
        x = self.fc1(x)
        x = F.relu(x)
        x = self.dropout2(x)
        x = self.fc2(x)
        output = F.log_softmax(x, dim=1)
        return output


class FCN(nn.Module):
    def __init__(self, hidden: 128):
        super(FCN, self).__init__()
        self.dropout1 = nn.Dropout(0.25)
        self.dropout2 = nn.Dropout(0.5)
        self.fc1 = nn.Linear(784, hidden)
        self.fc2 = nn.Linear(hidden, 10)

    def forward(self, x):
        x = torch.flatten(x, 1)
        x = self.fc1(x)
        x = F.relu(x)
        x = self.dropout2(x)
        x = self.fc2(x)
        output = F.log_softmax(x, dim=1)
        return output

class BinnedLinear(nn.Module):
    __constants__ = ['in_features', 'out_features', 'nbins', 'bins', 'boundary']
    in_features: int
    out_features: int
    nbins: int
    boundary: int
    bins: Tensor
    weight: Tensor

    def __init__(self, in_features: int, out_features: int, nbins: int, boundary: int, bias: bool = True,
                 device=None, dtype=None) -> None:
        factory_kwargs = {'device': device, 'dtype': dtype}
        super(BinnedLinear, self).__init__()
        if nbins % 2 == 0:
            nbins+=1

        self.in_features = in_features
        self.out_features = out_features
        self.boundary = boundary
        self.nbins = nbins
        step = 2*self.boundary/(nbins-1)
        self.bins = Parameter(torch.arange(start=-boundary, end=self.boundary+step/2, step=step, **factory_kwargs))
        self.weight = Parameter(torch.empty((out_features, in_features, nbins), **factory_kwargs))
        if bias:
            self.bias = Parameter(torch.empty(out_features, nbins, **factory_kwargs))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self) -> None:
        # Setting a=sqrt(5) in kaiming_uniform is the same as initializing with
        # uniform(-1/sqrt(in_features), 1/sqrt(in_features)). For details, see
        # https://github.com/pytorch/pytorch/issues/57109
        init.kaiming_uniform_(self.weight, a=math.sqrt(5))
        if self.bias is not None:
            fan_in, _ = init._calculate_fan_in_and_fan_out(self.weight)
            bound = 1 / math.sqrt(fan_in) if fan_in > 0 else 0
            init.uniform_(self.bias, -bound, bound)

    def extra_repr(self) -> str:
        return 'in_features={}, out_features={}, bias={}, nbins={}'.format(
            self.in_features, self.out_features, self.bias is not None, self.nbins
        )

    def forward(self, input: Tensor) -> Tensor:
        softmaxed_w = torch.softmax(self.weight, -1)
        calculated_w = torch.sum(softmaxed_w * self.bins.detach(), -1)

        softmaxed_b = torch.softmax(self.bias, -1)
        calculated_b = torch.sum(softmaxed_b * self.bins.detach(), -1)

        return F.linear(input, calculated_w, calculated_b)

    def weight_loss(self):
        softmaxed_w = torch.softmax(self.weight, -1)
        ent_w = self.entropy(softmaxed_w)

        softmaxed_b = torch.softmax(self.bias, -1)
        ent_b = self.entropy(softmaxed_b)

        return (ent_w + ent_b)*100

    def entropy(self, values):
        return (-values*torch.log(values)).mean()

    def to(self, *args, device, **kwargs):
        super(self).to(device)
        self.bins.to(device)

class FCNBinned(nn.Module):
    def __init__(self, hidden: int = 128, nbins: int = 11, boundary: float = 3, **kwargs):
        super(FCNBinned, self).__init__()
        if nbins % 2 == 0:
            nbins+=1

        self.nbins = nbins
        self.boundary = boundary
        self.dropout1 = nn.Dropout(0.25)
        self.dropout2 = nn.Dropout(0.5)
        self.fc1 = BinnedLinear(784, hidden, nbins, boundary, **kwargs)
        self.fc2 = BinnedLinear(hidden, 10, nbins, boundary, **kwargs)

    def forward(self, x):
        x = torch.flatten(x, 1)
        x = self.fc1(x)
        x = F.relu(x)
        x = self.dropout2(x)
        x = self.fc2(x)
        output = F.log_softmax(x, dim=1)
        return output

    def weight_loss(self):
        return self.fc1.weight_loss() + self.fc1.weight_loss()
