from __future__ import print_function
import os

import torch
import torch.nn.functional as F
from tqdm import tqdm

ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__)))

def train(*args, with_closure: bool = False, **kwargs):
    if with_closure:
        return train_withclosure(*args, **kwargs)
    return train_simple(*args, **kwargs)

def train_simple(args, model, device, train_loader, optimizer, epoch, callbacks):
    model.train()
    result = {}
    for batch_idx, (data, target) in tqdm(enumerate(train_loader), total=len(train_loader)):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)

        if hasattr(model, 'weight_loss'):
            loss += model.weight_loss()

        loss.backward()
        optimizer.step()

        pred = output.argmax(dim=1, keepdim=True)  # get the index of the max log-probability
        result['correct'] = pred.eq(target.view_as(pred)).sum().item()
        result['total'] = len(pred)
        if batch_idx % args.log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tAccu: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), result['correct']/result['total']))
            if args.dry_run:
                break

        for c in callbacks:
            c(result)

def train_withclosure(cfg, model, device, train_loader, optimizer, epoch, callbacks):
    result = {}
    for batch_idx, (data, target) in tqdm(enumerate(train_loader), total=len(train_loader)):
        model.train()
        data, target = data.to(device), target.to(device)
        def closure():
            optimizer.zero_grad()
            output = model(data)
            loss = F.nll_loss(output, target)
            if hasattr(model, 'weight_loss'):
                loss += model.weight_loss()
            # loss = loss*loss
            loss.backward()
            result['loss'] = loss.item()

            pred = output.argmax(dim=1, keepdim=True)  # get the index of the max log-probability
            result['correct'] = pred.eq(target.view_as(pred)).sum().item()
            result['total'] = len(pred)

            return loss
        optimizer.step(closure)
        if batch_idx % cfg.log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tAccu: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), result['correct']/result['total']))
            if cfg.dry_run:
                break

        for c in callbacks:
            c(result)

def test(model, device, test_loader):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            test_loss += F.nll_loss(output, target, reduction='sum').item()  # sum up batch loss
            pred = output.argmax(dim=1, keepdim=True)  # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(test_loader.dataset)
    accuracy = 100. * correct / len(test_loader.dataset)

    print('Test set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)'.format(
        test_loss, correct, len(test_loader.dataset), accuracy))

    return accuracy
